from conans import ConanFile, AutoToolsBuildEnvironment, VisualStudioBuildEnvironment, tools
import os
import shutil
import glob


#ifeq ($(OS),WNT)
#$(call gb_ExternalProject_get_state_target,nss,build): $(call gb_ExternalExecutable_get_dependencies,python)
#	$(call gb_Trace_StartRange,nss,EXTERNAL)
#	$(call gb_ExternalProject_run,build,\
#		$(if $(MSVC_USE_DEBUG_RUNTIME),USE_DEBUG_RTL=1,BUILD_OPT=1) \
#		$(if $(gb_Module_CURRENTMODULE_SYMBOLS_ENABLED), \
#			MOZ_DEBUG_SYMBOLS=1 \
#			MOZ_DEBUG_FLAGS=" " \
#			OPT_CODE_SIZE=0) \
#		MOZ_MSVCVERSION=9 OS_TARGET=WIN95 \
#		$(if $(filter X86_64,$(CPUNAME)),USE_64=1) \
#		LIB="$(ILIB)" \
#		XCFLAGS="-arch:SSE $(SOLARINC)" \
#		$(MAKE) -j1 nss_build_all RC="rc.exe $(SOLARINC)" \
#			NSINSTALL='$(call gb_ExternalExecutable_get_command,python) $(SRCDIR)/external/nss/nsinstall.py' \
#			NSS_DISABLE_GTESTS=1 \
#	,nss)
#	$(call gb_Trace_EndRange,nss,EXTERNAL)
#
#else # OS!=WNT
## make sure to specify NSPR_CONFIGURE_OPTS as env (before make command), so nss can append it's own defaults
## OTOH specify e.g. CC and NSINSTALL as arguments (after make command), so they will overrule nss makefile values
#$(call gb_ExternalProject_get_state_target,nss,build): $(call gb_ExternalExecutable_get_dependencies,python)
#	$(call gb_Trace_StartRange,nss,EXTERNAL)
#	$(call gb_ExternalProject_run,build,\
#		$(if $(filter ANDROID FREEBSD LINUX MACOSX,$(OS)),$(if $(filter X86_64,$(CPUNAME)),USE_64=1)) \
#		$(if $(filter ANDROID,$(OS)),$(if $(filter AARCH64,$(CPUNAME)),USE_64=1)) \
#		$(if $(filter iOS,$(OS)),$(if $(filter ARM64,$(CPUNAME)),USE_64=1)) \
#		$(if $(filter MACOSX,$(OS)),\
#			$(if $(filter-out POWERPC,$(CPUNAME)),MACOS_SDK_DIR=$(MACOSX_SDK_PATH)) \
#			NSS_USE_SYSTEM_SQLITE=1) \
#		$(if $(filter LINUX,$(OS)),$(if $(ENABLE_DBGUTIL),,BUILD_OPT=1)) \
#		$(if $(filter SOLARIS,$(OS)),NS_USE_GCC=1) \
#		$(if $(CROSS_COMPILING),\
#			$(if $(filter MACOSXPOWERPC,$(OS)$(CPUNAME)),CPU_ARCH=ppc) \
#			$(if $(filter iOS-ARM,$(OS)-$(CPUNAME)),CPU_ARCH=arm) \
#			NSPR_CONFIGURE_OPTS="--build=$(BUILD_PLATFORM) --host=$(HOST_PLATFORM)") \
#		NSDISTMODE=copy \
#		$(MAKE) -j1 AR="$(AR)" \
#			RANLIB="$(RANLIB)" \
#			NMEDIT="$(NM)edit" \
#			COMMA=$(COMMA) \
#			CC="$(CC)$(if $(filter ANDROID,$(OS)), -D_PR_NO_LARGE_FILES=1 -DSQLITE_DISABLE_LFS=1)" CCC="$(CXX)" \
#			$(if $(CROSS_COMPILING),NSINSTALL="$(call gb_ExternalExecutable_get_command,python) $(SRCDIR)/external/nss/nsinstall.py") \
#			$(if $(filter ANDROID,$(OS)),OS_TARGET=Android OS_TARGET_RELEASE=16 ARCHFLAG="" DEFAULT_COMPILER=clang ANDROID_NDK=$(ANDROID_NDK_HOME) ANDROID_TOOLCHAIN_VERSION=$(ANDROID_GCC_TOOLCHAIN_VERSION) ANDROID_PREFIX=$(HOST_PLATFORM) ANDROID_SYSROOT=$(ANDROID_NDK_HOME)/sysroot ANDROID_TOOLCHAIN=$(ANDROID_BINUTILS_PREBUILT_ROOT)) \
#			NSS_DISABLE_GTESTS=1 \
#			nss_build_all \
#		&& rm -f $(call gb_UnpackedTarball_get_dir,nss)/dist/out/lib/*.a \
#		
#	,nss)
#	$(call gb_Trace_EndRange,nss,EXTERNAL)
#
#endif


class GetTextConan(ConanFile):
    name = "nss"
    version = "3.54"
    description = "Mozilla nss with nspr"
    topics = ("conan", "mozilla", "nss", "nspr")
    url = "https://gitlab.com/hernad/conan-nss"
    homepage = "https://gitlab.com/hernad/conan-nss"
    license = "MPL 2.0"
    settings = "os_build", "arch_build", "compiler"
    #exports_sources = ["patches/*.patch"]
    #requires = [("libiconv/1.16", "private")]


    _autotools = None

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _is_msvc(self):
        return self.settings.compiler == "Visual Studio"

    @property
    def _make_args(self):

        if self.settings.arch_build == "x86_64":
            args = [ "USE_64=1" ]
        else:
            args = [ "USE_64=0" ]

        #arch = {
        #        "x86_64": "x86_64",
        #        "x86": "i686",
        #}
        #if self.settings.os == "Windows":
        #    build_triplet = host_triplet = "{}-w64-mingw32".format(arch[str(self.settings.arch)])    
        #else:
        
        #build_triplet = host_triplet = "x86_64" #"{}-Linux".format(arch[str(self.settings.arch)])

        #args.extend(["NSPR_CONFIGURE_OPTS=\"--build={} --host={}\"".format(build_triplet, host_triplet)])

        #    "NSDISTMODE=copy",
        #    "NMEDIT=nmedit",
        #    "NSINSTALL=python $(SRCDIR)/external/nss/nsinstall.py",
        #    ,


        #'NSPR_CONFIGURE_OPTS="--build=x64 --host=x64"',

        #args.extend([
        #    "NSDISTMODE=copy",
        #     
        #    "nss_build_all"
        #])
        args.extend([
            "NSS_DISABLE_GTESTS=1",
            "nss_build_all", 
            "-j1"
        ])

        return args

    def configure(self):
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

    def build_requirements(self):

        if self._is_msvc:
        	self.build_requires("gyp_installer/20190821@bincrafters/stable")
        self.build_requires("ninja_installer/1.9.0@bincrafters/stable")

        #gyp_installer/20181217@bincrafters/stable
        #gyp_installer/20190423@bincrafters/stable
        #gyp_installer/20190821@bincrafters/stable

        if tools.os_info.is_windows:
            self.build_requires("mozilla-build/3.3@hernad/stable")
            #if "CONAN_BASH_PATH" not in os.environ and tools.os_info.detect_windows_subsystem() != "msys2":
            #    self.build_requires("msys2/20190524")
        #if self._is_msvc:
        #    self.build_requires("automake/1.16.1")

    def source(self):
        #http://www.myrkraverk.com/blog/2019/08/building-mozilla-nss-on-windows-10/

        #os.mkdir(self._source_subfolder)
        tools.get(**self.conan_data["sources"][self.version])
        extracted_dir = "nss-" + self.version
        os.rename(extracted_dir, self._source_subfolder)

    def _configure_autotools(self):
        if self._autotools:
            return self._autotools
        #libiconv_prefix = self.deps_cpp_info["libiconv"].rootpath
        #libiconv_prefix = tools.unix_path(libiconv_prefix) if tools.os_info.is_windows else libiconv_prefix


        build = None
        host = None
        rc = None

        args = []
        #args.extend(["--disable-shared", "--disable-static"])
        if self._is_msvc:
            # INSTALL.windows: Native binaries, built using the MS Visual C/C++ tool chain.
            build = False
            if self.settings.arch_build == "x86":
                host = "i686-w64-mingw32"
                rc = "windres --target=pe-i386"
            elif self.settings.arch_build == "x86_64":
                host = "x86_64-w64-mingw32"
                rc = "windres --target=pe-x86-64"
            automake_perldir = tools.unix_path(os.path.join(self.deps_cpp_info['automake'].rootpath, "bin", "share", "automake-1.16"))
            args.extend(["CC=%s/compile cl -nologo" % automake_perldir,
                         "LD=link",
                         "NM=dumpbin -symbols",
                         "STRIP=:",
                         "AR=%s/ar-lib lib" % automake_perldir,
                         "RANLIB=:"])
            if rc:
                args.extend(['RC=%s' % rc, 'WINDRES=%s' % rc])
        self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)
        if self._is_msvc:
            self._autotools.flags.append("-FS")
        self._autotools.configure(args=args, build=build, host=host)
        return self._autotools


    #[hernad@centos7-kvm-local nss]$ ./build.sh --help
    #Usage: build.sh [-h] [-c|-cc] [-v] [-j <n>] [--gyp|-g] [--opt|-o]
    #        [-t <x64|ia32|...>|--target=<x64|ia32|...>]
    #        [--clang|--gcc|--msvc] [--scan-build[=dir]] [--disable-tests]
    #        [--pprof] [--asan] [--msan] [--ubsan[=bool,shift,...]
    #        [--fuzz[=tls|oss]] [--sancov[=edge|bb|func|...]]
    #        [--emit-llvm] [--no-zdefs] [--static] [--ct-verif]
    #        [--nspr|--with-nspr=<include>:<lib>|--system-nspr]
    #        [--system-sqlite] [--enable-fips] [--enable-libpkix]
    #        [--mozpkix-only] [-D<gyp-option>]
    #        [--rebuild]

    #This script builds NSS with gyp and ninja.
    #NSS #build tool options:

    #-h               display this help and exit
    #-c               clean before build
    #-cc              clean without building
    #-v               verbose build
    #-j <n>           run at most <n> concurrent jobs
    #--gyp|-g         force a rerun of gyp
    #--opt|-o         do an opt build
    #--target|-t      specify target architecture (e.g., ia32, x64, aarch64)
    #--clang          build with clang and clang++
    #--gcc            build with gcc and g++
    #--msvc           build with MSVC
    #--scan-build     run the build with scan-build
    #                 --scan-build=<dir> sets the output path for scan-build
    #--disable-tests  don't build tests and corresponding cmdline utils
    #--pprof          build with gperftool support
    #--asan           enable address sanitizer
    #--msan           enable memory sanitizer
    #--ubsan          enable undefined behavior sanitizer
    #                 --ubsan=bool,shift,... sets specific UB sanitizers
    #--fuzz           build fuzzing targets (this always enables static builds)
    #                 --fuzz=tls to enable TLS fuzzing mode
    #                 --fuzz=oss to build for OSS-Fuzz
    #--sancov         do sanitize coverage builds
    #                 --sancov=func sets coverage to function level for example
    #--emit-llvm      emit LLVM bitcode while building
    #                 (requires the gold linker, use clang-3.8 for SAW)
    #--no-zdefs       don't set -Wl,-z,defs
    #--static         create static libraries and use static linking
    #--ct-verif       build with valgrind for ct-verif
    #--nspr           force a rebuild of NSPR
    #--nspr-test-build when building NSPR also build its tests
    #--nspr-test-run  when building NSPR tests also run its tests
    #--nspr-only      exit after building NSPR
    #--with-nspr      use the NSPR build at the given locations
    #                 --with-nspr=<include>:<lib> sets include and lib paths
    #--system-nspr    attempt to use system nspr
    #                 shorthand for --with-nspr=/usr/include/nspr:
    #--system-sqlite  use system sqlite
    #--enable-fips    enable FIPS checks
    #--enable-libpkix make libpkix part of the build
    #--mozpkix-only   build only static mozpkix and mozpkix-test libraries
    #                 support for this build option is limited
    #--disable-keylog disable support for logging key data to a file specified
    #                 by the SSLKEYLOGFILE environment variable
    #-D<gyp-option>   pass an option directly to gyp
    #--rebuild        build again using the last set of options provided
    #                 (all other arguments are ignored if --rebuild is used)

    def build(self):
        #for patch in self.conan_data["patches"][self.version]:
        #    tools.patch(**patch)   
        with tools.vcvars(self.settings) if self._is_msvc else tools.no_op():
            with tools.environment_append(VisualStudioBuildEnvironment(self).vars) if self._is_msvc else tools.no_op():
                with tools.chdir(os.path.join(self._source_subfolder, "nss")):
                    if self._is_msvc:
                        #env_build = self._configure_autotools()
                        #if [ "${platform%-*}" = "MINGW32_NT" -o "${platform%-*}" = "MINGW64_NT" ]; then
                        #tools.replace_in_file("build.sh", "MINGW32_NT", "MSYS_NT")
                        #with tools.environment_append(env_build.vars):
                        #target: x64, ia32
                        #with tools.environment_append({"UNAME_SYSTEM": "MINGW32_NT"}):
                        # --target=x64
                        tools.run_in_windows_bash(self, "echo $PATH")
                        tools.run_in_windows_bash(self, "./build.sh -v --opt --disable-tests --msvc") # self is a conanfile instance

                        #self.run("bash.exe --login -i -c \"cd " + os.path.join(self._source_subfolder, "nss") + " ; ./build.sh -v --opt --disable-tests --msvc\"")
                    else:
                        #self.run("echo $PATH")
                        #self.run("python coreconf/detect_host_arch.py")
                        self.run("./build.sh -v --opt --disable-tests")



    def package(self):
        #self.copy(pattern="COPYING", dst="licenses", src=self._source_subfolder)

        # dist/public/nss, dist/public/dbm
        self.copy(pattern="*", dst="include", src=os.path.join(self._source_subfolder, "dist", "public"))

        # dist/Release/include,bin,share,lib
        self.copy(pattern="*", dst="", src=os.path.join(self._source_subfolder, "dist", "Release"))

        #with tools.vcvars(self.settings) if self._is_msvc else tools.no_op():
        #    with tools.environment_append(VisualStudioBuildEnvironment(self).vars) if self._is_msvc else tools.no_op():
        #        with tools.chdir(os.path.join(self._source_subfolder, "dist", "Release")):
        #            env_build = self._configure_autotools()
        #            env_build.install()
        #tools.rmdir(os.path.join(self.package_folder, 'share'))
        #tools.rmdir(os.path.join(self.package_folder, 'lib'))
        #tools.rmdir(os.path.join(self.package_folder, 'include'))

    def package_id(self):
        self.info.include_build_settings()
        del self.info.settings.compiler

    def package_info(self):
        bindir = os.path.join(self.package_folder, "bin")
        self.output.info('Appending PATH environment variable: {}'.format(bindir))
        self.env_info.PATH.append(bindir)


